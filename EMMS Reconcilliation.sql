	select transactionid from msats.interval_meter_read where interval_date between '2019-11-17' and '2019-11-23' 
	and nmi = 'QBGHW00010' and nmi_suffix in ('B1',  'B2') order by transactionid asc )
	
	select * ,
	(case 
		WHEN transactionid = (select top 1 transactionid from msats.interval_meter_read where interval_date between '2019-11-17' and '2019-11-23' and nmi = 'QBGHW00010' and nmi_suffix in ('B1',  'B2') order by transactionid desc ) THEN 'FINAL'
		WHEN transactionid = (select top 1 transactionid from msats.interval_meter_read where interval_date between '2019-11-17' and '2019-11-23' and nmi = 'QBGHW00010' and nmi_suffix in ('B1',  'B2') ) THEN 'PRELIM'
	end) as STATUS 
	from msats.interval_meter_read
	where interval_date between '2019-11-17' and '2019-11-23' and nmi = 'QKYHW00730' and nmi_suffix in ('Q1', 'Q2')
	  

	
	----working setgendata
	----returns raw data based on weekno
	select tt.SETTLEMENTDATE, tt.CONTRACTYEAR, tt.WEEKNO, tt.PERIODID, tt.RUNNO, tt.PARTICIPANTID, tt.DUID, tt.GENERGY, tt.EXPENERGY, tt.STATUS, tt.LASTCHANGED from
	(select a.settlementdate, CONTRACTYEAR, WEEKNO, PERIODID, RUNNO, PARTICIPANTID, DUID, GENERGY, EXPENERGY, LASTCHANGED, STATUS from emms.SETGENDATA
	--select * from emms.SETGENDATA
	inner join 
	(select emms.BILLINGDAYTRK.SETTLEMENTDATE, emms.BILLINGDAYTRK.CONTRACTYEAR, emms.BILLINGDAYTRK.WEEKNO, emms.BILLINGDAYTRK.RUNNO, emms.BILLINGRUNTRK.STATUS  from emms.BILLINGDAYTRK
	inner join emms.BILLINGRUNTRK on emms.BILLINGDAYTRK.BILLRUNNO = emms.BILLINGRUNTRK.BILLRUNNO AND emms.BILLINGDAYTRK.WEEKNO = emms.BILLINGRUNTRK.WEEKNO
	where emms.BILLINGRUNTRK.STATUS in ('PRELIM', 'FINAL') and BILLINGDAYTRK.CONTRACTYEAR >=2020 and BILLINGRUNTRK.CONTRACTYEAR >= 2020
	) a 
	on emms.setgendata.VERSIONNO = a.RUNNO and emms.setgendata.settlementdate = a.settlementdate
	where WEEKNO = 42 and CONTRACTYEAR = 2020) tt
	INNER JOIN (SELECT settlementdate, PERIODID, versionno, MAX(lastchanged) as LASTCHANGED from emms.SETGENDATA group by settlementdate, periodid, VERSIONNO) groupedtt
	on tt.SETTLEMENTDATE = groupedtt.SETTLEMENTDATE and tt.RUNNO = groupedtt.versionno and tt.PERIODID = groupedtt.PERIODID and tt.LASTCHANGED = groupedtt.LASTCHANGED
	order by tt.SETTLEMENTDATE, tt.Periodid, tt.WEEKNO, tt.STATUS, tt.LASTCHANGED



	
	----working setcpdata
	----returns raw data based on weekno
	select tt.SETTLEMENTDATE, tt.CONTRACTYEAR, tt.WEEKNO, tt.PERIODID, tt.RUNNO, tt.PARTICIPANTID, tt.TCPID, tt.IGENERGY, tt.XNENERGY, STATUS from
	(select a.settlementdate, CONTRACTYEAR, WEEKNO, PERIODID, RUNNO, PARTICIPANTID, TCPID, IGENERGY, XNENERGY, LASTCHANGED, STATUS from emms.SETCPDATA
	--select * from emms.SETGENDATA
	inner join 
	(select emms.BILLINGDAYTRK.SETTLEMENTDATE, emms.BILLINGDAYTRK.CONTRACTYEAR, emms.BILLINGDAYTRK.WEEKNO, emms.BILLINGDAYTRK.RUNNO, emms.BILLINGRUNTRK.STATUS  from emms.BILLINGDAYTRK
	inner join emms.BILLINGRUNTRK on emms.BILLINGDAYTRK.BILLRUNNO = emms.BILLINGRUNTRK.BILLRUNNO AND emms.BILLINGDAYTRK.WEEKNO = emms.BILLINGRUNTRK.WEEKNO
	where emms.BILLINGRUNTRK.STATUS in ('PRELIM', 'FINAL') and BILLINGDAYTRK.CONTRACTYEAR >=2020 and BILLINGRUNTRK.CONTRACTYEAR >= 2020
	) a 
	on emms.SETCPDATA.VERSIONNO = a.RUNNO and emms.SETCPDATA.settlementdate = a.settlementdate
	where WEEKNO = 42 and CONTRACTYEAR = 2020
	) tt
	INNER JOIN (SELECT settlementdate, PERIODID, versionno, MAX(lastchanged) as LASTCHANGED from emms.SETGENDATA group by settlementdate, periodid, VERSIONNO) groupedtt
	on tt.SETTLEMENTDATE = groupedtt.SETTLEMENTDATE and tt.RUNNO = groupedtt.versionno and tt.PERIODID = groupedtt.PERIODID and tt.LASTCHANGED = groupedtt.LASTCHANGED
	order by tt.SETTLEMENTDATE, tt.Periodid, tt.WEEKNO, tt.STATUS



	----working setgendata
	----returns summary data based on weekno
	select tt.DUID, sum(tt.GENERGY) as GENERGY, sum(tt.EXPENERGY) as EXPENERGY, tt.STATUS from
	(select a.settlementdate, CONTRACTYEAR, WEEKNO, PERIODID, RUNNO, PARTICIPANTID, DUID, GENERGY, EXPENERGY, LASTCHANGED, STATUS from emms.SETGENDATA
	--select * from emms.SETGENDATA
	inner join 
	(select emms.BILLINGDAYTRK.SETTLEMENTDATE, emms.BILLINGDAYTRK.CONTRACTYEAR, emms.BILLINGDAYTRK.WEEKNO, emms.BILLINGDAYTRK.RUNNO, emms.BILLINGRUNTRK.STATUS  from emms.BILLINGDAYTRK
	inner join emms.BILLINGRUNTRK on emms.BILLINGDAYTRK.BILLRUNNO = emms.BILLINGRUNTRK.BILLRUNNO AND emms.BILLINGDAYTRK.WEEKNO = emms.BILLINGRUNTRK.WEEKNO
	where emms.BILLINGRUNTRK.STATUS in ('PRELIM', 'FINAL') and BILLINGDAYTRK.CONTRACTYEAR >=2020 and BILLINGRUNTRK.CONTRACTYEAR >= 2020
	) a 
	on emms.setgendata.VERSIONNO = a.RUNNO and emms.setgendata.settlementdate = a.settlementdate
	where WEEKNO = 42 and CONTRACTYEAR = 2020 ) tt
	INNER JOIN (SELECT settlementdate, PERIODID, versionno, MAX(lastchanged) as LASTCHANGED from emms.SETGENDATA group by settlementdate, periodid, VERSIONNO) groupedtt
	on tt.SETTLEMENTDATE = groupedtt.SETTLEMENTDATE and tt.RUNNO = groupedtt.versionno and tt.PERIODID = groupedtt.PERIODID and tt.LASTCHANGED = groupedtt.LASTCHANGED
	group by tt.CONTRACTYEAR, tt.WEEKNO, tt.PARTICIPANTID, tt.DUID, tt.STATUS
	order by status
	

		
	----working setcpdata
	----returns summary data based on weekno
	select tt.TCPID, SUM(tt.IGENERGY) as IGENERGY, SUM(tt.XNENERGY) as XNENERGY, STATUS from
	(select a.settlementdate, CONTRACTYEAR, WEEKNO, PERIODID, RUNNO, PARTICIPANTID, TCPID, IGENERGY, XNENERGY, LASTCHANGED, STATUS from emms.SETCPDATA
	--select * from emms.SETGENDATA
	inner join 
	(select emms.BILLINGDAYTRK.SETTLEMENTDATE, emms.BILLINGDAYTRK.CONTRACTYEAR, emms.BILLINGDAYTRK.WEEKNO, emms.BILLINGDAYTRK.RUNNO, emms.BILLINGRUNTRK.STATUS  from emms.BILLINGDAYTRK
	inner join emms.BILLINGRUNTRK on emms.BILLINGDAYTRK.BILLRUNNO = emms.BILLINGRUNTRK.BILLRUNNO AND emms.BILLINGDAYTRK.WEEKNO = emms.BILLINGRUNTRK.WEEKNO
	where emms.BILLINGRUNTRK.STATUS in ('PRELIM', 'FINAL') and BILLINGDAYTRK.CONTRACTYEAR >=2020 and BILLINGRUNTRK.CONTRACTYEAR >= 2020
	) a 
	on emms.SETCPDATA.VERSIONNO = a.RUNNO and emms.SETCPDATA.settlementdate = a.settlementdate
	where WEEKNO = 42 and CONTRACTYEAR = 2020
	) tt
	INNER JOIN (SELECT settlementdate, PERIODID, versionno, MAX(lastchanged) as LASTCHANGED from emms.SETGENDATA group by settlementdate, periodid, VERSIONNO) groupedtt
	on tt.SETTLEMENTDATE = groupedtt.SETTLEMENTDATE and tt.RUNNO = groupedtt.versionno and tt.PERIODID = groupedtt.PERIODID and tt.LASTCHANGED = groupedtt.LASTCHANGED
	group by tt.CONTRACTYEAR, tt.WEEKNO, tt.PARTICIPANTID, tt.TCPID, tt.STATUS
	order by status



















	--
		----returns raw data based on weekno
	--select tt.SETTLEMENTDATE, tt.CONTRACTYEAR, tt.WEEKNO, tt.PERIODID, tt.RUNNO, tt.PARTICIPANTID, tt.DUID, tt.GENERGY, tt.EXPENERGY, tt.STATUS, tt.LASTCHANGED from
	
	
	select * from
	(select  a.settlementdate, * from emms.SETGENDATA
	--a.settlementdate, CONTRACTYEAR, WEEKNO, PERIODID, RUNNO, PARTICIPANTID, DUID, GENERGY, EXPENERGY, LASTCHANGED, STATUS from emms.SETGENDATA
	--select * from emms.SETGENDATA
	inner join 
	(select emms.BILLINGDAYTRK.SETTLEMENTDATE, emms.BILLINGDAYTRK.CONTRACTYEAR, emms.BILLINGDAYTRK.WEEKNO, emms.BILLINGDAYTRK.RUNNO, emms.BILLINGRUNTRK.STATUS  from emms.BILLINGDAYTRK
	inner join emms.BILLINGRUNTRK on emms.BILLINGDAYTRK.BILLRUNNO = emms.BILLINGRUNTRK.BILLRUNNO AND emms.BILLINGDAYTRK.WEEKNO = emms.BILLINGRUNTRK.WEEKNO
	where emms.BILLINGRUNTRK.STATUS in ('PRELIM', 'FINAL') 
	) a 
	on emms.setgendata.VERSIONNO = a.RUNNO and emms.setgendata.settlementdate = a.settlementdate
	where WEEKNO = 42  
	--order by a.SETTLEMENTDATE, Periodid, WEEKNO, STATUS, LASTCHANGED
	) tt
	INNER JOIN (SELECT settlementdate, PERIODID, versionno, MAX(lastchanged) as LASTCHANGED from emms.SETGENDATA group by settlementdate, periodid, VERSIONNO) groupedtt
	on tt.SETTLEMENTDATE = groupedtt.SETTLEMENTDATE and tt.RUNNO = groupedtt.versionno and tt.PERIODID = groupedtt.PERIODID and tt.LASTCHANGED = groupedtt.LASTCHANGED
	order by tt.SETTLEMENTDATE, tt.Periodid, tt.WEEKNO, tt.STATUS, tt.LASTCHANGED
	

		select tt.SETTLEMENTDATE, tt.CONTRACTYEAR, tt.WEEKNO, tt.PERIODID, tt.RUNNO, tt.PARTICIPANTID, tt.DUID, tt.GENERGY, tt.EXPENERGY, tt.STATUS, tt.LASTCHANGED from
	(select  a.settlementdate, CONTRACTYEAR, WEEKNO, PERIODID, RUNNO, PARTICIPANTID, DUID, GENERGY, EXPENERGY, LASTCHANGED, STATUS from emms.SETGENDATA

	inner join 
	(select  emms.BILLINGDAYTRK.SETTLEMENTDATE, emms.BILLINGDAYTRK.CONTRACTYEAR, emms.BILLINGDAYTRK.WEEKNO, emms.BILLINGDAYTRK.RUNNO, emms.BILLINGRUNTRK.STATUS, BILLINGRUNTRK.BILLRUNNO from emms.BILLINGDAYTRK
	inner join emms.BILLINGRUNTRK on emms.BILLINGDAYTRK.BILLRUNNO = emms.BILLINGRUNTRK.BILLRUNNO AND emms.BILLINGDAYTRK.WEEKNO = emms.BILLINGRUNTRK.WEEKNO
	where emms.BILLINGRUNTRK.STATUS in ('PRELIM', 'FINAL') and BILLINGDAYTRK.CONTRACTYEAR >=2020 and BILLINGRUNTRK.CONTRACTYEAR >= 2020
	) a 
	on emms.setgendata.VERSIONNO = a.RUNNO and emms.setgendata.settlementdate = a.settlementdate
	where WEEKNO = 42 and CONTRACTYEAR = 2020) tt
	INNER JOIN (SELECT settlementdate, PERIODID, versionno, MAX(lastchanged) as LASTCHANGED from emms.SETGENDATA group by settlementdate, periodid, VERSIONNO ) groupedtt
	--order by SETTLEMENTDATE desc, PERIODID) groupedtt
	on tt.SETTLEMENTDATE = groupedtt.SETTLEMENTDATE and tt.RUNNO = groupedtt.versionno and tt.PERIODID = groupedtt.PERIODID and tt.LASTCHANGED = groupedtt.LASTCHANGED
	order by tt.SETTLEMENTDATE, tt.Periodid, tt.WEEKNO, tt.STATUS, tt.LASTCHANGED

