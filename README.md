# README #

Reconciliation between msats and aemo. Foundation assets included. 

### What is this repository for? ###

Excel workbook will be required to be updated, once a year to update the AEMO settlement calendar information and whenever a new DUID is set up.

### How do I get set up? ###

TradingRisk team to update workbook.

### Contribution guidelines ###

All code to be updated and submitted via bitbucket and released to Forward trading team and settlements.

### Who do I talk to? ###

Aaron Smith